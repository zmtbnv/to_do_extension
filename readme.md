# Todo Chrome extension
- Simple and light extension for task management in daily life.

## Features:
1. Simple UI with 2 tabs for Today and Later tasks.
2. Task can be moved from Today to Later tabs for better task management.
3. Each task is editable
4. All information is stored locally
