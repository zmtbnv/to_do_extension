var gulp = require('gulp');
var stylus = require('gulp-stylus'),
    concat = require('gulp-concat'),
    copy = require('gulp-copy'),
    uglify = require('gulp-uglify'),
    ngAnnotate = require('gulp-ng-annotate'),
    minifyHTML = require('gulp-minify-html'),
    htmlify = require('gulp-angular-htmlify');

gulp.task('stylus', function () {
  gulp.src('./dev/css/todo.styl')
    .pipe(stylus({compress: true}))
    .pipe(gulp.dest('./dev/css/'));
});

gulp.task('css', function () {
  gulp.src([
    './dev/js/lib/angular-xeditable/dist/css/xeditable.css',
    './dev/css/todo.css'
    ])
    .pipe(concat('todo.css'))
    .pipe(gulp.dest('./dev/css/'));
});

gulp.task('js', function () {
  gulp.src([
    'dev/js/lib/angular/angular.min.js',
    'dev/js/lib/angular-xeditable/dist/js/xeditable.min.js',
    'dev/js/app/**/*.js'
    ])
    .pipe(concat('todo.js'))
    .pipe(ngAnnotate())
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest('dist/js/'))
});

gulp.task('html', function() {
  gulp.src(['dev/*.html'])
    .pipe(htmlify())
    .pipe(minifyHTML({quotes: true}))
    .pipe(gulp.dest('dist'))
});

gulp.task('copy', function () {
  return gulp.src([
    'dev/css/todo.css',
    'dev/img/**'
  ])
  .pipe(copy('dist/', {
    prefix: 1
  }));
});

gulp.task('watch', function () {
  gulp.watch(['./dev/css/*.styl'], ['stylus', 'css']);
});

gulp.task('default', ['watch']);
gulp.task('dist', ['js', 'html', 'copy']);