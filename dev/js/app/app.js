﻿var todoApp = angular.module('ToDo', ['xeditable']);

todoApp.run(function(editableOptions) {
  editableOptions.buttons = 'no';
});