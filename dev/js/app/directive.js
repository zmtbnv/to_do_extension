todoApp
  .directive('hoverShow', function() {
    return {
      link: function(scope, el, attrs) {
        el
          .bind('mouseenter', function() {
            //TODO: find by ID or class
            el.children().find('div').addClass('active');
          })
          .bind('mouseleave', function() {
            el.children().find('div').removeClass('active');
          });
      }
    }
  })
  .directive('autoFocus', function($timeout) {
    return {
      restrict: 'AC',
      scope: {
        control: '='
      },
      link: function(scope, el, attrs) {
        scope.autofocus = function() {
          $timeout(function() {
            el[0].focus();
          }, 800);
        }
        scope.autofocus();
      }
    }
  });