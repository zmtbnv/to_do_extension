﻿todoApp
  .controller("todoCtrl", function($scope, $timeout, storage) {
    $scope.storage = storage;

    /*----------  CRUD  ----------*/
    $scope.$watch('storage.data', function() {
      $scope.todoList = $scope.storage.data;
    });

    $scope.storage.findAll(function(data) {
      $scope.todoList = data;
      $scope.$apply();
    });

    $scope.add = function() {
      if ($scope.newContent !== '') {

        $scope.item = {
          content: $scope.newContent,
          label: $scope.tabLabel
        };

        storage.add($scope.item);
        $scope.newContent = '';
      }
    }

    $scope.remove = function(todo) {
      storage.remove(todo);
    }

    $scope.removeAll = function() {
      storage.removeAll();
    }

    $scope.toggleCompleted = function(todo) {
      storage.toggle(todo);
    }

    $scope.moveTodo = function(todo) {
      storage.move(todo);
    }

    $scope.validate = function(content) {
      if (content == "") {
        return "Should not be empty";
      } else {
        // TODO: sync only if not empty
        storage.sync();
      }
    }

    /*----------  tabs  ----------*/

    $scope.tabLabel = 0;
    $scope.tabs = [{
      title: 'Today',
      url: 'today-tpl.html',
      label: 0
        // total: 0 // TODO: fix number
    }, {
      title: 'Later',
      url: 'later-tpl.html',
      label: 1
        // total: 0 // TODO: fix number
    }];

    $scope.currentTab = $scope.tabs[0].url;

    $scope.changeTab = function(tab) {
      $scope.currentTab = tab.url;
      $scope.tabLabel = tab.label;
    }

    $scope.setActiveTab = function(tabUrl) {
      return tabUrl === $scope.currentTab;
    }
  });
