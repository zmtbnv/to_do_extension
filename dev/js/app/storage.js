angular.module('ToDo').service('storage', function($q) {
  var _this = this;
  this.data = [];
  this.total = [];

  this.findAll = function(callback) {
    chrome.storage.sync.get('todo', function(keys) {
      if (keys.todo != null) {
        _this.data = keys.todo;
        callback(_this.data);
      }
    });
  }

  this.toggle = function(todo) {
    this.sync();
  }

  this.sync = function() {
    chrome.storage.sync.set({
      todo: this.data
    }, function() {
      // console.info('Data is stored in Chrome storage');
    });
    this.setBadge();
  }

  this.add = function(item) {
    var todo = {
      id: makeid(),
      content: item.content,
      label: item.label,
      completed: false,
      time: new Date()
    };

    this.data.push(todo);
    this.sync();
  }

  this.remove = function(todo) {
    this.data.splice(this.data.indexOf(todo), 1);
    this.sync();
  }

  this.removeAll = function() {
    this.data = [];
    this.sync();
  }

  this.move = function(todo) {
    var item = this.data.splice(this.data.indexOf(todo), 1)[0];

    if (item.label === 0) {
      todo.label = 1;
    } else {
      todo.label = 0;
    }

    this.data.push(todo);
    this.sync();
  }

  this.setBadge = function() {
    chrome.browserAction.setBadgeText({
      text: this.findTaskNum()
    });
  }

  this.findTaskNum = function() {
    var keys = [];
    angular.forEach(this.data, function(val, key){
      if (!val.completed) {
        keys.push(key);
      }
    });
    return keys.length.toString();
  }

  function makeid() {
    var text = "",
      possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
});
